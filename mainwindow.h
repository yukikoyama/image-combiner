#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>

#include <string>
#include <vector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT    
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
    void updateDynamicForms();

private slots:
    void on_startButton_clicked();

    void on_n_editingFinished();

private:
    Ui::MainWindow *ui;

    int n;

    int wOutput;
    int hOutput;

    int start;
    int end;

    int nDigits;

    std::vector<QLineEdit *> edits;

    std::vector<std::string> iPrefixes;
    std::string oPrefix;
    std::string iPath;
    std::string oPath;
    std::string suffix;
};

#endif // MAINWINDOW_H
