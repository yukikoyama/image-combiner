#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QImage>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cassert>

using namespace std;

string getFilePath(string path, string suffix, string name, int digits, int i) {
    ostringstream filePath;
    filePath << path + name;
    filePath << setfill('0') << setw(digits) << i;
    filePath << suffix;
    return filePath.str();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    updateDynamicForms();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateDynamicForms()
{
    n = ui->n->text().toInt();

    // clear current forms
    foreach (QLineEdit *edit, edits) {
        ui->gridLayout->removeWidget(edit);
        edit->deleteLater();
    }
    edits.clear();

    // create new forms
    for (int i = 0; i < n; ++ i) {
        QLineEdit *newEdit = new QLineEdit(this);
        ui->gridLayout->addWidget(newEdit, 0, i);
        edits.push_back(newEdit);
    }

    ui->gridLayout->update();
    update();
}

void MainWindow::on_startButton_clicked()
{
    iPrefixes.resize(n);

    for (int i = 0; i < n; ++ i) {
        iPrefixes[i] = edits[i]->text().toStdString();
    }

    oPrefix      = ui->oPrefix->text().toStdString();

    oPath        = ui->path->text().toStdString();
    iPath        = ui->path->text().toStdString();

    suffix       = ui->suffix->text().toStdString();

    start   = ui->firstNum->text().toInt();
    end     = ui->lastNum->text().toInt();
    nDigits = ui->nDigits->text().toInt();
    assert (start <= end);

    wOutput = ui->w->text().toInt();
    hOutput = ui->h->text().toInt();

    int d  = wOutput / n;

    vector<int> xs;
    for (int i = 0; i <= n; ++ i) {
        int x = i * d;
        xs.push_back(x);
    }

    for (int i = start; i <= end; ++ i) {
        cout << i << endl;

        vector<QImage> images;

        for (int j = 0; j < n; ++ j) {
            string path  = getFilePath(iPath, suffix, iPrefixes[j], nDigits, i);
            QImage image(QString(path.c_str()));
            images.push_back(image);

            // failed to open the input images
            if (image.isNull()) {
                cout << "Failed" << endl;
                return;
            }
        }

        string oFullPath = getFilePath(oPath, suffix, oPrefix, nDigits, i);
        QImage oImage(wOutput, hOutput, QImage::Format_ARGB32);

        int wInput  = images.front().width();
        int hInput  = images.front().height();

        for (int y = 0; y < hOutput; ++ y) {
            for (int j = 0; j < n; ++ j) {
                for (int x = xs[j]; x < xs[j + 1]; ++ x) {
                    int localX = wInput / 2 + x - xs[j] - d / 2;
                    int localY = hInput / 2 + y - hOutput / 2;
                    oImage.setPixel(x, y, images[j].pixel(localX, localY));
                }
            }
        }

        // save the output image
        oImage.save(oFullPath.c_str());
    }
}

void MainWindow::on_n_editingFinished()
{
    updateDynamicForms();
}
