# Goal
This GUI software provides a function of making a comparison video from several types of serial-numbered images.

# Input
Several types of serial-numbered images in the same directory.

# Output
Serial-numbered images, which can be converted to a video using other tools such as ffmpeg.

# Behavior
1. Import several types of serial-numbered images
1. Arrange them horizontally for each frame
1. Save each frame with a serial-numbered name

# Environments
This code requires the Qt library to compile.

# License
MIT License

# Contact
Yuki Koyama (koyama@is.s.u-tokyo.ac.jp)

# Other Information
This software was originally implemented for my master's thesis project.

